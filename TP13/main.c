/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: loloandrade
 *
 * Created on June 7, 2023, 11:54 AM
 */

#include <stdlib.h>

#include "puerto.h"
#include "teclado.h"
#include "hardware.h"

int main (void) 
{
    exportpins_all();
    setpinsdirect_out();
    
    int f_q = 0;
    while(f_q != 1)
    {
        f_q = leer_teclado();   
    }
    
    unexportpins_all();
    
    return (EXIT_SUCCESS);
}

