#include "puerto.h"
#include "hardware.h"

#define INIT_MASK 0b0000000000000001    // Máscara inicial que luego será "shifteada"

PUERTOS portd;      // Variable estática, ya que solo es utilizada por este archivo

void bitSet(char port, unsigned int bit)    
{
    int16_t setmask = INIT_MASK;    // Inicialización de la máscara setmask
    
    if( ( (((port == 'B') || (port == 'b'))  || ((port == 'A') || (port == 'a'))) &&  (bit < 8) )  ||  (((port == 'D') || (port == 'd')) && (bit < 16)))    // Validación
    {
        setmask = setmask<<bit;     // La máscara se "shiftea" según el número de bit seleccionado
        
        if((port == 'D') || (port == 'd'))      // Según el puerto seleccionado, se hace un or bitwise con setmask
        {
            (portd.puertod.portd) = setmask | (portd.puertod.portd);
        }
        else if ((port == 'B') || (port == 'b'))
        {
            (portd.puertoab.portb) = setmask | (portd.puertoab.portb);
        }
        else
        {
            (portd.puertoab.porta) = setmask | (portd.puertoab.porta);
            
            //IMPLEMENTACION HARDWARE//
            pinSet(bit);
            ///////////////////////////
            
        } 
    }
    else
    {
        printf("Se ingresó algún parámetro erróneo\n");     // Si se ingresó un parámetro inválido, mensaje de error
    }
   
    return;
}

void bitClr(char port, unsigned int bit)   
{
    int16_t clrmask = INIT_MASK;    // Inicialización de la máscara clrmask
    
    if( ( (((port == 'B') || (port == 'b'))  || ((port == 'A') || (port == 'a'))) &&  (bit < 8) )  ||  (((port == 'D') || (port == 'd')) && (bit < 16)))    // Validación
    {
        clrmask = clrmask<<bit;     // La máscara se "shiftea" según el número de bit seleccionado
        clrmask = ~clrmask;     // Se le aplica complemento a 1 a la máscara
        
        if((port == 'D') || (port == 'd'))      // Según el puerto seleccionado, se hace un and bitwise con clrmask     
        {
            (portd.puertod.portd) &= clrmask;       
        }
        else if ((port == 'B') || (port == 'b'))
        {
            (portd.puertoab.portb) &= clrmask;
        }
        else
        {
            (portd.puertoab.porta) &= clrmask;
            
            //IMPLEMENTACION HARDWARE//
            pinClr(bit);
            ///////////////////////////
            
        }
    }
    else
    {
        printf("Se ingresó algún parámetro erróneo\n");     // Si se ingresó un parámetro inválido, mensaje de error
    }
    
    return;
}

int bitGet(char port, unsigned int bit)     
{
    int16_t getmask = INIT_MASK;    // Inicialización de la máscara getmask
    int x = 0;      // La variable a devolver se inicializa en 0
    
    if( ( (((port == 'B') || (port == 'b'))  || ((port == 'A') || (port == 'a'))) &&  (bit < 8) )  ||  (((port == 'D') || (port == 'd')) && (bit < 16)))    // Validación 
    {
        getmask = getmask<<bit;     // La máscara se "shiftea" según el número de bit seleccionado
        
        if((port == 'D') || (port == 'd'))      // Se guarda en getmask el resultado del and bitwise entre getmask y el puerto seleccionado
        {
            getmask &= (portd.puertod.portd);  
        }
        else if ((port == 'B') || (port == 'b'))
        {
            getmask &= (portd.puertoab.portb);
        }
        else
        {
            getmask &= (portd.puertoab.porta);
        }
    }
    else
    {
        printf("Se ingresó algún parámetro erróneo\n");     // Si se ingresó un parámetro inválido, mensaje de error
    }
    
    if (getmask)    // Si getmask queda distinta de 0, x vale 1
    {
        x = 1;
    } 
    
    return x;
}

void bitToggle(char port, unsigned int bit)     
{
    if( ( (((port == 'B') || (port == 'b'))  || ((port == 'A') || (port == 'a'))) &&  ((bit < 8))  )  ||  (((port == 'D') || (port == 'd')) && (bit < 16)))     // Validación  
    {
	if(bitGet(port, bit))       // Con bitGet evalúo el estado del bit
	{
            bitClr(port, bit);      // Si estaba en 1, con bitClr lo pongo en 0
	}
	else
	{
            bitSet(port, bit);      // Si estaba en 0, con bitSet lo pongo en 1
	}
    }
    else
    {
        printf("Se ingresó algún parámetro erróneo\n");     // Si se ingresó un parámetro inválido, mensaje de error
    }
    
    return;
}

void maskOn(char port, int mascara)
{
    if (((port != 'A') && (port != 'a')) && ((port != 'B') && (port != 'b')) && ((port != 'D') && (port != 'd')))     // Validación
    {
      printf("No se seleccionó un puerto válido\n");    // Si se ingresó un puerto inválido, mensaje de error
    }
    else
    {    
        if(mascara <= 0xFF)
        {
            if ((port == 'A') || (port == 'a'))
            {
                portd.puertoab.porta |= mascara;    // Según el puerto indicado, se le aplica or bitwise con la máscara ingresada
            }
            else
            {
                portd.puertoab.portb |= mascara;
            }
        }
        else if ((mascara <= 0xFFFF) && ((port == 'D') || (port == 'd')))
        {
            portd.puertod.portd |= mascara;
        }        
        else
        {
            printf("Se aplicó una máscara mayor al tamaño del puerto %c\n", port);      // Si se ingresó una máscara inválida, mensaje de error
        }
    }         
    
    return;
}

void maskOff(char port, int mascara)
{
    mascara = ~mascara;     //  Hago el complemento a 1, ya que se deben apagar los que estén en 1 en la máscara
    
    if (((port != 'A') && (port != 'a')) && ((port != 'B') && (port != 'b')) && ((port != 'D') && (port != 'd'))) 
    {
        printf("No se seleccionó un puerto válido\n");    // Si se ingresó un puerto inválido, mensaje de error
    }
    else
    {    
        if(mascara <= 0xFF)
        {
            if ((port == 'A') || (port == 'a'))     
            {
                portd.puertoab.porta &= mascara;    // Según el puerto indicado, se le aplica and bitwise con el complemento a 1 de la máscara ingresada
            }
            else
            {
                portd.puertoab.portb &= mascara;
            }
        }
        else if ((mascara <= 0xFFFF) && ((port == 'D') || (port == 'd')))
        {
          
            portd.puertod.portd &= mascara;
        }        
        else
        {
            printf("Se aplicó una máscara mayor al tamaño del puerto %c\n", port);      // Si se ingresó una máscara inválida, mensaje de error
        }
    }         
    
    return;
}

void maskToggle(char port, int mascara)
{
    if (((port != 'A') && (port != 'a')) && ((port != 'B') && (port != 'b')) && ((port != 'D') && (port != 'd')))   // Validación
    {
      printf("No se seleccionó un puerto válido\n");    // Si se ingresó un puerto inválido, mensaje de error
    }
    else
    {    
        if(mascara <= 0xFF)
        {
            if ((port == 'A') || (port == 'a'))
            {
                portd.puertoab.porta ^= mascara;    // Según el puerto indicado, se le aplica xor bitwise con la máscara ingresada
            }
            else
            {
                portd.puertoab.portb ^= mascara;
            }
        }
        else if ((mascara <= 0xFFFF) && ((port == 'D') || (port == 'd')))
        {
           portd.puertod.portd ^= mascara;
        }        
        else
        {
            printf("Se aplicó una máscara mayor al tamaño del puerto %c\n", port);      // Si se ingresó una máscara inválida, mensaje de error
        }
    }         
    
    return; 
}