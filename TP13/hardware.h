/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   export.h
 * Author: lio2309
 *
 * Created on 7 de junio de 2023, 12:30
 */

#ifndef HARDWARE_H
#define HARDWARE_H

#include <stdio.h>
#include <stdlib.h>

void exportpins_all(void);
void setpinsdirect_out(void);
void pinSet(unsigned int bit);
void pinClr(unsigned int bit);
void unexportpins_all(void);

#endif /* HARDWARE_H */

