#include "puerto.h"
#include "printleds8bits.h"


void printleds8bits (void)
{
    int i;
    int bit;
	
    for(i=7; i >= 0 ;i--)
    {		
        printf("[");
	
	bit = bitGet('a', i);
		
	if(bit == 0)
	{
            printf(" ");		
	}
	else
	{
            printf("o");
	}

	printf("]");
    }

    printf("\n");
}