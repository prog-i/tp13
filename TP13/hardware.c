/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   export.c
 * Author: lio2309
 *
 * Created on 7 de junio de 2023, 12:28
 */

/*
 * 
 */
#include "hardware.h"

//static int pinnums[] = {17,4,18,23,24,25,22,27};
static char *pinnum[]={"17","4","18","23","24","25","22","27"};
static char command[33];

void exportpins_all(void)
{
    int i;
    
    FILE *handle_direction;
    int nWritten;
    //char *pinnum[]={"17","4","18","23","24","25","22","27"};
    
    for(i=0; i < 8 ;i++)
    {
        if((handle_direction = fopen("/sys/class/gpio/export","w")) == NULL)
    	{
            printf ("Cannot open Export file. Try again later.\n");
            exit(-1);              
    	}
		
        if((nWritten=fputs(pinnum[i], handle_direction)) == -1)
        {
            printf ("Cannot write pinnum. Try again later.\n");
            exit(-1);
	
        }
        fclose(handle_direction);
    }
    
}

void setpinsdirect_out(void)
{
    int i;
    
    FILE *handle_direction;
    int nWritten;
    
    for(i=0; i < 8 ;i++)
    {
        sprintf(command, "/sys/class/gpio/gpio%s/direction", pinnum[i]);
        if((handle_direction = fopen(command,"w")) == NULL)
    	{
            printf ("Cannot open Direction file. Try again later.\n");
            exit(-1);              
    	}
		
        if((nWritten=fputs("out", handle_direction)) == -1)
        {
            printf ("Cannot write on Direction file. Try again later.\n");
            exit(-1);
	
        }
        fclose(handle_direction);
    }
}

void pinSet(unsigned int bit)
{
    
    FILE *handle_direction;
    int nWritten;
    
    sprintf(command, "/sys/class/gpio/gpio%s/value", pinnum[bit]);
    if((handle_direction = fopen(command,"w")) == NULL)
    {
        printf ("Cannot open Value file. Try again later.\n");
        exit(-1);              
    }
    
    if((nWritten=fputs("1", handle_direction)) == -1)
    {
        printf ("Cannot write on Value file. Try again later.\n");
        exit(-1);
	
    }
    fclose(handle_direction);
    
}

void pinClr(unsigned int bit)
{
    
    FILE *handle_direction;
    int nWritten;
    
    sprintf(command, "/sys/class/gpio/gpio%s/value", pinnum[bit]);
    if((handle_direction = fopen(command,"w")) == NULL)
    {
        printf ("Cannot open Value file. Try again later.\n");
        exit(-1);              
    }
    
    if((nWritten=fputs("0", handle_direction)) == -1)
    {
        printf ("Cannot write on Value file. Try again later.\n");
        exit(-1);
	
    }
    fclose(handle_direction);
    
}

void unexportpins_all(void)
{
    int i;
    
    FILE *handle_direction;
    int nWritten;
    //char *pinnum[]={"17","4","18","23","24","25","22","27"};
    
    for(i=0; i < 8 ;i++)
    {
        if((handle_direction = fopen("/sys/class/gpio/unexport","w")) == NULL)
    	{
            printf ("Cannot open Unexport file. Try again later.\n");
            exit(-1);              
    	}
		
        if((nWritten=fputs(pinnum[i], handle_direction)) == -1)
        {
            printf ("Cannot write pinnum. Try again later.\n");
            exit(-1);
	
        }
        fclose(handle_direction);
    }
    
}