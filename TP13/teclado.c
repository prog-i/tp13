#include "puerto.h"
#include "teclado.h"
#include "printleds8bits.h"

int leer_teclado (void)			//interpreta la entrada del teclado
{
    char c;
    char j;
    unsigned int n;
    int f_q = 0;
    int i;
    int f_valido = 0;                   //se utiliza para verificar si se realizo correctamente alguna funcion para imprimir los leds
    int contador = 0;
    
    while((j = getchar()) != '\n')
    {
        c = j;                                            //se utiliza otra variable para que no agarre el enter como caracter a analizar
        contador++;
    }

    if(contador > 1)                                        
    {
        printf("Se ingresaron demasiados caracteres\n");    //caso donde se presiona mas de un caracter
        contador = 0;
    }
    else if(contador < 1)
    {
        printf("No se ingreso ningun caracter\n");        //caso donde solo se presiona el enter
    }       
    else
    {
        if((c == 't') || (c == 'T'))
        {
            for (i=0; i < 8 ; i++)
            {
                bitToggle('A', i);
                f_valido = 1;
            }     
        }
        else if((c == 'c') || (c == 'C'))
        {
            for (i=0; i < 8 ;i++)
            {
                bitClr('A', i);
            } 
            
            f_valido = 1;
        }
        else if((c == 's') || (c == 'S'))
        {
            for (i=0; i < 8 ;i++)
            {
                bitSet('A', i);
            } 
            
            f_valido = 1;    
        }
        else if((c == 'q') || (c == 'Q'))
        {
            f_q = 1;
        }
        else if((c >= '0') && (c < '8'))
        {      
            n = c - '0';
            bitToggle('A', n);
            f_valido = 1;
        }
        else
        {
            printf("Se ingreso un caracter invalido\n");       //llega aca si el caracter ingresado es un 8,9 o una letra distinta a los comandos
        }
    }
    
    if(f_valido == 1)
    {
        printleds8bits();
    }
    
    return f_q;
}
